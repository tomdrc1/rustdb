use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize)]
pub struct Forum
{
    pub name : String,
    pub author : String,
    pub date_created : String,
    pub content: String
}

impl Forum
{
    pub fn new(name : String, author : String, date_created : String, content: String) -> Forum
    {
        Forum{name: name, author: author, date_created: date_created, content: content}
    }
}