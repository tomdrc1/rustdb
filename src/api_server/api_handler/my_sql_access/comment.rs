use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize)]
pub struct Comment
{
    pub author: String,
    pub content: String,
    pub date_created: String
}

impl Comment
{
    pub fn new(author: String, content: String, date_created: String) -> Comment
    {
        Comment{author: author, content: content, date_created: date_created}
    }
}