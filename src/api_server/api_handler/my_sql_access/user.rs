use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize)]
pub struct User
{
    pub username : String,
    pub password : String,
    pub email : String
}

impl User
{
    pub fn new(username : String, password : String, email : String) -> User
    {
        User{username: username, password: password, email: email}
    }

    pub fn print_myself(&self)
    {
        println!("username: {}, password: {}, email: {}", self.username, self.password, self.email);
    }
}