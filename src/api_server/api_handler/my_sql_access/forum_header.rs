use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize)]
pub struct ForumHeader
{
    pub name : String,
    pub username : String,
    pub date_created : String
}

impl ForumHeader
{
    pub fn new(name : String, username : String, date_created : String) -> ForumHeader
    {
        ForumHeader{name: name, username: username, date_created: date_created}
    }
}