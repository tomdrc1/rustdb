extern crate mysql;
extern crate bcrypt;

pub mod user;
pub mod forum_header;
pub mod forum;
pub mod comment;

pub struct MySqlAccess
{
    pool: mysql::Pool
}

impl MySqlAccess
{
    pub fn new(connection_url: String) -> MySqlAccess
    {
        MySqlAccess{pool: mysql::Pool::new(connection_url).unwrap()}
    } 
    
    pub fn get_all_users(&self) -> Vec<String>
    {
        let mut users: Vec<String> = Vec::new();

        for row in self.pool.prep_exec("SELECT USERNAME FROM USERS;", ()).unwrap()
        {
            //Username, password, email
            let username = mysql::from_row::<String>(row.unwrap());

            users.push(username);
        }

        users
    }

    pub fn get_users_by_search(&self, query: &String) -> Vec<String>
    {
        let mut users: Vec<String> = Vec::new();

        for row in self.pool.prep_exec(format!("SELECT USERNAME FROM USERS WHERE USERNAME LIKE '%{}%';", query), ()).unwrap()
        {
            //Username, password, email
            let username = mysql::from_row::<String>(row.unwrap());

            users.push(username);
        }

        users
    }

    pub fn get_user_by_name(&self, username: &String) -> user::User
    {
        let mut user_data: (String, String, String) = ("".to_string(), "".to_string(), "".to_string());

        for row in self.pool.prep_exec(format!("SELECT USERNAME, PASSWORD, EMAIL FROM USERS WHERE USERNAME='{}';", username), ()).unwrap()
        {
            //Username, password, email
            user_data = mysql::from_row::<(String, String, String)>(row.unwrap());
        }

        user::User::new(user_data.0, user_data.1, user_data.2)
    }

    pub fn does_user_exist(&self, username: &String) -> bool
    {
        for _row in self.pool.prep_exec(format!("SELECT * FROM USERS WHERE USERNAME='{}';", username), ()).unwrap()
        {
            return true; //If we got here then there is a user.
        }

        false
    }

    pub fn delete_user(&self, username: &String) -> bool
    {
        if let Err(e) = self.pool.prep_exec(format!("DELETE FROM USERS WHERE USERNAME='{}';", username), ())
        {
            println!("{}", e);
            return false;
        }

        true
    }

    pub fn login_user(&self, username: &String, password: &String) -> bool
    {
        for row in self.pool.prep_exec(format!("SELECT PASSWORD FROM USERS WHERE USERNAME='{}';", username),()).unwrap()
        {
            let db_password = mysql::from_row::<String>(row.unwrap());

            if bcrypt::verify(password, &db_password).unwrap()
            {
                return true;
            }
        }
        false

    }

    pub fn insert_user(&self, username: &String, password: &String, email: &String) -> bool
    {
        if let Err(e) = self.pool.prep_exec(format!("INSERT INTO USERS(USERNAME, PASSWORD, EMAIL) VALUES('{}', '{}', '{}');", username, password, email), ())
        {
            println!("{}", e);
            return false;
        }

        true
    }

    pub fn get_forum_headers(&self) -> Vec<forum_header::ForumHeader>
    {
        let mut headers: Vec<forum_header::ForumHeader> = Vec::new();

        for row in self.pool.prep_exec("SELECT NAME, AUTHOR, DATE_CREATED FROM FORUMS;", ()).unwrap()
        {
            //name, username, date_created
            let header_data = mysql::from_row::<(String, String, String)>(row.unwrap());

            let header = forum_header::ForumHeader::new(header_data.0, header_data.1, header_data.2);
            headers.push(header);
        }

        headers
    }

    pub fn create_forum(&self, name: &String, author: &String, date_created: &String, category: &String, content: &String) -> bool
    {
        if let Err(e) = self.pool.prep_exec(format!("INSERT INTO FORUMS(NAME, AUTHOR, DATE_CREATED, CATEGORY, CONTENT) VALUES('{}', '{}', '{}', '{}', '{}');", name, author, date_created, category, content), ())
        {
            println!("{}", e);
            return false;
        }

        true
    }

    pub fn create_forum_comment(&self, author: &String, content: &String, date_created: &String, forum_id: i64) -> bool
    {
        if let Err(e) = self.pool.prep_exec(format!("INSERT INTO FORUM_COMMENT(AUTHOR, CONTENT, DATE_CREATED, FORUM_ID) VALUES('{}', '{}', '{}', {});", author, content, date_created, forum_id), ())
        {
            println!("{}", e);
            return false;
        }

        true
    }

    pub fn does_forum_exist(&self, name: &String, author: &String) -> bool
    {
        for _row in self.pool.prep_exec(format!("SELECT * FROM FORUMS WHERE NAME='{}' AND AUTHOR='{}';",name ,author), ()).unwrap()
        {
            return true; //If we got here then there is a forum.
        }

        false
    }

    pub fn get_forum_by_name_and_creator(&self, name: &String, author: &String) -> forum::Forum
    {
        let mut forum_data: (String, String, String, String) = ("".to_string(), "".to_string(), "".to_string(), "".to_string());

        for row in self.pool.prep_exec(format!("SELECT NAME, AUTHOR, DATE_CREATED, CONTENT FROM FORUMS WHERE NAME='{}' AND AUTHOR='{}';", name, author), ()).unwrap()
        {
            //name, author, date_created, content
            forum_data = mysql::from_row::<(String, String, String, String)>(row.unwrap());
        }

        forum::Forum::new(forum_data.0, forum_data.1, forum_data.2, forum_data.3)
    }

    pub fn get_forum_id(&self, name: &String, author: &String) -> i64
    {
        let mut id: i64 = 0;

        for row in self.pool.prep_exec(format!("SELECT ID FROM FORUMS WHERE NAME='{}' AND AUTHOR='{}';", name, author), ()).unwrap()
        {
            id = mysql::from_row::<i64>(row.unwrap());
        }

        id
    }

    pub fn get_forum_comments(&self, forum_id: i64) -> Vec<comment::Comment>
    {
        let mut comments: Vec<comment::Comment> = Vec::new();

        for row in self.pool.prep_exec(format!("SELECT AUTHOR, CONTENT, DATE_CREATED FROM FORUM_COMMENT WHERE FORUM_ID={}", forum_id), ()).unwrap()
        {
            //author, content, date_creatd
            let comment_data = mysql::from_row::<(String, String, String)>(row.unwrap());

            let comment = comment::Comment::new(comment_data.0, comment_data.1, comment_data.2);
            comments.push(comment);
        }

        comments
    }

    pub fn is_admin(&self, username: &String) -> bool
    {
        let mut admin = false;

        for row in self.pool.prep_exec(format!("SELECT ADMIN FROM USERS WHERE USERNAME='{}'", username), ()).unwrap()
        {
            admin = mysql::from_row::<bool>(row.unwrap());
        }

        admin
    }

    pub fn get_forum_headers_by_search(&self, query: &String, category: &String) -> Vec<forum_header::ForumHeader>
    {
        let mut headers: Vec<forum_header::ForumHeader> = Vec::new();

        for row in self.pool.prep_exec(format!("SELECT NAME, AUTHOR, DATE_CREATED FROM FORUMS WHERE NAME LIKE '%{}%' AND CATEGORY LIKE '%{}%';", query, category), ()).unwrap()
        {
            //name, username, date_created
            let header_data = mysql::from_row::<(String, String, String)>(row.unwrap());

            let header = forum_header::ForumHeader::new(header_data.0, header_data.1, header_data.2);
            headers.push(header);
        }

        headers
    }

    pub fn change_password(&self, username: &String, password: &String) -> bool
    {
        if let Err(e) = self.pool.prep_exec(format!("UPDATE USERS SET PASSWORD='{}' WHERE USERNAME='{}';", password, username), ())
        {
            println!("{}", e);
            return false;
        }

        true
    }

    pub fn delete_forum(&self, forum_name: &String, forum_creator: &String) -> bool
    {
        if let Err(e) = self.pool.prep_exec(format!("DELETE FROM FORUMS WHERE NAME='{}' AND AUTHOR='{}';", forum_name, forum_creator), ())
        {
            println!("{}", e);
            return false;
        }

        true
    }

    pub fn delete_forum_comments(&self, forum_id: i64) -> bool
    {
        if let Err(e) = self.pool.prep_exec(format!("DELETE FROM FORUM_COMMENT WHERE FORUM_ID={};", forum_id), ())
        {
            println!("{}", e);
            return false;
        }

        true
    }

    pub fn delete_forums_by_author(&self, author: &String) -> bool
    {
        for row in self.pool.prep_exec(format!("SELECT NAME, AUTHOR FROM FORUMS WHERE AUTHOR='{}';", author), ()).unwrap()
        {
            //name, author
            let forum_data = mysql::from_row::<(String, String)>(row.unwrap());

            let temp_id = self.get_forum_id(&forum_data.0, &forum_data.1);

            self.delete_forum_comments(temp_id);
        }

        if let Err(e) = self.pool.prep_exec(format!("DELETE FROM FORUMS WHERE AUTHOR='{}';", author), ())
        {
            println!("{}", e);
            return false;
        }

        true
    }

    pub fn delete_comments_by_author(&self, author: &String) -> bool
    {
        if let Err(e) = self.pool.prep_exec(format!("DELETE FROM FORUM_COMMENT WHERE AUTHOR='{}';", author), ())
        {
            println!("{}", e);
            return false;
        }

        true
    }
}
