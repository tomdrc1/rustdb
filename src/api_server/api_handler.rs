mod my_sql_access;

extern crate serde;
extern crate jsonwebtoken;
extern crate rand;
extern crate regex;
extern crate bcrypt;
extern crate base64;

use serde_json;
use std::net::TcpStream;
use std::io::prelude::*;
use std::collections::HashMap;
use std::cell::RefCell;
use jsonwebtoken::{encode, Header};
use rand::Rng;
use std::sync::{Mutex};
use regex::Regex;

const BUFFER_SIZE: usize = 57344;
const ASCII: [char; 62] = [
    'a', 'b', 'c', 'd',
    'e', 'f', 'g', 'h',
    'i', 'j', 'k', 'l', 
    'm', 'n', 'o', 'p', 
    'q', 'r', 's', 't', 
    'u', 'v', 'w', 'x', 
    'y', 'z', 'A', 'B', 
    'C', 'D', 'E', 'F', 
    'G', 'H', 'I', 'J', 
    'K', 'L', 'M', 'N', 
    'O', 'P', 'Q', 'R', 
    'S', 'T', 'U', 'V', 
    'W', 'X', 'Y', 'Z', 
    '1', '2', '3', '4', 
    '5', '6', '7', '8', 
    '9', '0'];

const FORUM_CATEGORIES: [&str; 4] = ["Developer", "Food", "Games", "Tech"];

pub struct APIHandler
{
    sql_access: Mutex<my_sql_access::MySqlAccess>,
    connected_users: Mutex<RefCell<HashMap<String, String>>>
}

impl APIHandler
{
    /// Returns a new APIServer instance
    /// 
    /// # Arguments
    /// 
    /// * `my_sql_connection_string` - A string that holds the connection string for a mysql server
    /// 
    /// # Example
    /// ```
    /// 
    /// ```
    pub fn new(my_sql_connection_string: String) -> APIHandler
    {
        let sql_access = my_sql_access::MySqlAccess::new(my_sql_connection_string);

        APIHandler{ sql_access: Mutex::new(sql_access), connected_users: Mutex::new(RefCell::new(HashMap::new()))}
    }

    /// This function will handle the client connection
    /// 
    /// # Arguments
    /// 
    /// * `stream` - A TcpStream connection that is mutable, so we can read and write to it. This is the conenction socket to the user.
    pub fn handle_client(&self, mut stream: TcpStream)
    {
        let mut buffer = [0; BUFFER_SIZE];
        stream.read(&mut buffer).unwrap();

        let mut msg = String::from_utf8_lossy(&buffer[..]).to_string();
        msg = msg.trim_matches(char::from(0)).to_string(); // Get rid of NULL
        println!("{}", msg);

        if !self.is_login_or_signup_or_amount(&msg)
        {
            if !self.is_user_connected(&msg)
            {
                stream.write("HTTP/1.1 401 Unauthorized\r\n\r\n".as_bytes()).unwrap();
                stream.flush().unwrap();
                return;
            }
        }

        if msg.starts_with("GET")
        {
            self.handle_get_request(&mut stream, msg);
        }
        else if msg.starts_with("POST")
        {
            self.handle_post_request(&mut stream, msg);
        }
        else if msg.starts_with("DELETE")
        {
            self.handle_delete_request(&mut stream, msg);
        }
        else if msg.starts_with("PUT")
        {
            self.handle_put_request(&mut stream, msg);
        }
    }

    /// Will check if the request is login or signup, if it is we don't need to authnticate the request.println!
    /// 
    /// # Arguments
    /// 
    /// * `msg_from_client` - A String reference the holds the message from the client
    fn is_login_or_signup_or_amount(&self, msg_from_client: &String) -> bool
    {
        let parsed_msg: Vec<&str> = msg_from_client.split(" ").collect();
        let parsed_request: Vec<&str> = parsed_msg[1].split("/").collect();

        parsed_request[2] == "LOGIN" || parsed_request[2] == "REGISTER" || parsed_request[2] == "LOGGED_AMOUNT"
    }

    /// Returns if the category is a valid one
    /// 
    /// # Arguments
    /// * `category` - A String reference that holds the submitted category
    fn is_category_valid(&self, category: &String) -> bool
    {
        FORUM_CATEGORIES.contains(&category.as_str())
    }

    /// Will check if the user is connected by it's cookie
    /// 
    /// # Arguments
    /// 
    /// * `msg_from_client` - A String refrence that holds the message we got from the client
    fn is_user_connected(&self, msg_from_client: &String) -> bool
    {
        let signature = self.get_cookie_value(msg_from_client);

        let connected_users = self.connected_users.lock().unwrap();
        let users = connected_users.borrow_mut();

        users.contains_key(&signature)
    }

    /// Returns if the username is valid or not
    /// 
    /// # Arguments
    /// 
    /// * `username` - A String reference that holds the username
    fn is_valid_username(&self, username: &String) -> bool
    {
        let re = Regex::new(r"^[a-zA-Z0-9]+([_ -]?[a-zA-Z0-9])*$").unwrap();

        re.is_match(username)
    }

    /// Returns if the password is valid or not
    /// 
    /// # Arguments
    /// 
    /// * `password` - A String reference that holds the password
    fn is_valid_password(&self, password: &String) -> bool
    {
        let re = Regex::new("^[-!$%^&*()_+|~=`{}:\";'<>?,./A-Za-z0-9]*$").unwrap();

        re.is_match(password)
    }

    /// Returns if the email is valid or not
    /// 
    /// # Arguments
    /// 
    /// * `email` - A String reference that holds the email
    fn is_valid_email(&self, email: &String) -> bool
    {
        let re = Regex::new(r"^[a-zA-Z0-9]+*@[a-zA-Z0-9]+*\.[a-zA-Z0-9]+*$").unwrap();

        re.is_match(email)
    }

    /// Returns the value the cookie from the user holds. This will usually be the signature so that we know that the user is actually authorized to make API calls
    /// 
    /// # Arguments
    /// 
    /// * `msg_from_client` - A String refrence that holds the message that the client has sent
    fn get_cookie_value(&self, msg_from_client: &String) -> String
    {
        let parsed_msg: Vec<&str> = msg_from_client.split("Cookie:").collect();
        
        // Incase there is no cookie 
        if parsed_msg.len() < 2
        {
            return "".to_string();
        }
        
        let parsed_cookie: Vec<&str> = parsed_msg[1].split("\n").collect();

        parsed_cookie[0].trim().to_string()
    }

    /// Returns a String containing the username
    /// 
    /// # Arguments
    /// * `signature` - A String reference that contains the signature that we want to get the username from
    fn get_username_from_signature(&self, signature: &String) -> String
    {
        let connected_users = self.connected_users.lock().unwrap();
        let users = connected_users.borrow_mut();

        let decoded_data = base64::decode(users.get(signature).unwrap()).unwrap();

        let json_data: serde_json::Value = serde_json::from_str(std::str::from_utf8(&decoded_data).unwrap()).unwrap();

        json_data["username"].as_str().unwrap().to_string()
    }

    /// This function will handle the DELETE request type.
    /// 
    /// # Arguments
    /// 
    /// * `stream` - A TcpStream connection to the user. A mutable refrence so we could borrow and write and read at ease.
    /// * `msg_from_client` - A String that holds the messsage from the client.
    fn handle_delete_request(&self, stream: &mut TcpStream, msg_from_client: String)
    {
        let parsed_msg: Vec<&str> = msg_from_client.split(" ").collect();
        let parsed_request: Vec<&str> = parsed_msg[1].split("/").collect();

        let msg_to_client: String = self.handle_delete_request_type(parsed_request, &msg_from_client);

        stream.write(msg_to_client.as_bytes()).unwrap();
        stream.flush().unwrap();
    }

    /// This function will handle the subtype of the DELETE request (delete user, post, etc.)
    /// 
    /// # Arguments
    /// 
    /// * `parsed_request` - A &str vector that holds the parsed reqeust (The part that comes after the DELETE)
    fn handle_delete_request_type(&self, parsed_request: Vec<&str>, msg_from_client: &String) -> String
    {
        let type_of_request = parsed_request[2];

        if type_of_request == "USER"
        {
            let username = parsed_request[3].to_string();
            return self.delete_user(username);
        }
        else if type_of_request == "DELETE_FORUM"
        {
            let parsed_data: Vec<&str> = parsed_request[3].split("&").collect();
            let forum_name = parsed_data[0].to_string().replace("%20", " ").replace("%27", "\\'").replace("%22", "\\\"");
            let forum_creator = parsed_data[1].to_string();
            let requested_user = self.get_username_from_signature(&self.get_cookie_value(msg_from_client));

            return self.delete_forum(forum_name, forum_creator, requested_user);
        }

        "HTTP/1.1 400 BAD REQUEST\r\n\r\n".to_string()
    }

    /// This function will delete the user, and will return the corret HTTP message to return to the user
    /// 
    /// # Arguments
    /// 
    /// * `username` - A string that holds the username of the user we want to delete.
    fn delete_user(&self, username: String) -> String
    {
        let sql_access = self.sql_access.lock().unwrap();
        if !sql_access.does_user_exist(&username)
        {
            return "HTTP/1.1 404 NOT FOUND\r\n\r\n".to_string();
        }

        if sql_access.is_admin(&username)
        {
            return "HTTP/1.1 401 Unauthorized\r\n\r\n".to_string();
        }
        sql_access.delete_comments_by_author(&username);
        sql_access.delete_forums_by_author(&username);
        sql_access.delete_user(&username);
        "HTTP/1.1 200 OK\r\n\r\n".to_string()
    }

    /// Returns a String with the correct HTTP code. This will also delete the forum's comments
    /// 
    /// # Arguments
    /// * `forum_name` - A String that holds the name of the forum
    /// * `forum_creator` - A String that holds the name of the creator of the forum
    /// * `requested_user` - A String that holds the user that made the request, this will check if the user is an admin as well. If not only the user that created the forum can delete it
    fn delete_forum(&self, forum_name: String, forum_creator: String, requested_user: String) -> String
    {   
        let sql_access = self.sql_access.lock().unwrap();

        if !sql_access.does_forum_exist(&forum_name, &forum_creator)
        {
            return "HTTP/1.1 404 NOT FOUND\r\n\r\n".to_string();
        }

        if forum_creator != requested_user && !sql_access.is_admin(&requested_user)
        {
            return "HTTP/1.1 401 Unauthorized\r\n\r\n".to_string();
        }

        let forum_id = sql_access.get_forum_id(&forum_name, &forum_creator);

        if !sql_access.delete_forum_comments(forum_id)
        {
            return "HTTP/1.1 500 Internal Server Error\r\n\r\n".to_string();
        }

        if !sql_access.delete_forum(&forum_name, &forum_creator)
        {
            return "HTTP/1.1 500 Internal Server Error\r\n\r\n".to_string();
        }

        "HTTP/1.1 200 OK\r\n\r\n".to_string()
    }

    /// This function will handle the post request from the user
    /// 
    /// # Arguments
    /// 
    /// * `stream` - A TcpStream connection to the user. A mutable refrence so we could borrow and write and read at ease.
    /// * `msg_from_client` - A String that holds the messsage from the client.
    fn handle_post_request(&self, stream: &mut TcpStream, msg_from_client: String)
    {
        let parsed_msg: Vec<&str> = msg_from_client.split(" ").collect();
        let parsed_request: Vec<&str> = parsed_msg[1].split("/").collect();

        let msg_to_client: String = self.handle_post_request_type(parsed_request, &msg_from_client);

        stream.write(msg_to_client.as_bytes()).unwrap();
        stream.flush().unwrap();
    }

    /// Will handle the subtype of the POST request. (LOGIN, REGISTER, etc.)
    /// 
    /// # Argumnets 
    /// 
    /// * `parsed_request` - A &str vector that holds the parsed reqeust (The part that comes after the POST)
    /// * `msg_from_client` - A String that holds the original message the client has sent, because the message holds JSON data we need.
    fn handle_post_request_type(&self, parsed_request: Vec<&str>, msg_from_client: &String) -> String
    {
        let type_of_request = parsed_request[2];
        let content_of_msg: Vec<&str> = msg_from_client.split("\r\n\r\n").collect();
        
        let data = content_of_msg[1].to_string().replace("\n", "\\n");
        println!("{}", data);
        let res: serde_json::Value = match serde_json::from_str(&data)
        {
            Ok(json_value) => json_value,
            Err(_) => serde_json::Value::Null
        };

        if res.is_null()
        {
            return "HTTP/1.1 400 BAD REQUEST\r\n\r\n".to_string();
        }

        if type_of_request == "REGISTER"
        {
            let username = res["username"].as_str().unwrap().to_string().replace("\'", "\\\'");
            let password = res["password"].as_str().unwrap().to_string().replace("\'", "\\\'");
            let email = res["email"].as_str().unwrap().to_string().replace("\'", "\\\'");

            return self.create_user(username, password, email);
        }
        else if type_of_request == "LOGIN"
        {
            let username = res["username"].as_str().unwrap().to_string().replace("\'", "\\\'");
            let password = res["password"].as_str().unwrap().to_string().replace("\'", "\\\'");

            return self.login_user(username, password);
        }
        else if type_of_request == "LOGOUT"
        {
            let signature = self.get_cookie_value(msg_from_client);

            return self.logout_user(&signature);
        }
        else if type_of_request == "CREATE_FORUM"
        {
            let name = res["name"].as_str().unwrap().to_string().replace("\'", "\\\'");
            let author = res["author"].as_str().unwrap().to_string().replace("\'", "\\\'");
            let date_created = res["date_created"].as_str().unwrap().to_string().replace("\'", "\\\'");
            let category = res["category"].as_str().unwrap().to_string().replace("\'", "\\\'");
            let content = res["content"].as_str().unwrap().to_string().replace("\'", "\\\'");

            return self.create_forum(name, author, date_created, category, content);
        }
        else if type_of_request == "CREATE_FORUM_COMMENT"
        {
            let author = res["author"].as_str().unwrap().to_string().replace("\'", "\\\'");
            let content = res["content"].as_str().unwrap().to_string().replace("\'", "\\\'");
            let date_created = res["date_created"].as_str().unwrap().to_string().replace("\'", "\\\'");
            let forum_id = res["forum_id"].as_i64().unwrap();

            return self.create_forum_comment(author, content, date_created, forum_id);
        }

        "HTTP/1.1 400 BAD REQUEST\r\n\r\n".to_string()
    }   

    /// Will create a user in the DB and will return the correct message (success, failed)
    /// 
    /// # Arguments
    /// * `username` - A String that holds the username
    /// * `password` - A String tha holds the password
    /// * `email` - A String that holds the email
    fn create_user(&self, username: String, password: String, email: String) -> String
    {
        let msg = "HTTP/1.1 201 CREATED\r\n\r\n".to_string();

        if username.is_empty() || password.is_empty() || email.is_empty()
        {
            return "HTTP/1.1 404 NOT FOUND\r\n\r\n".to_string();
        }

        if username.len() < 3 || !self.is_valid_username(&username)
        {
            return "HTTP/1.1 412 Precondition Failed\r\n\r\nBAD USERNAME".to_string();
        }
        
        if password.len() < 8 || !self.is_valid_password(&password)
        {
            return "HTTP/1.1 412 Precondition Failed\r\n\r\nBAD PASSWORD".to_string();
        }

        if !self.is_valid_email(&email)
        {
            return "HTTP/1.1 412 Precondition Failed\r\n\r\nBAD EMAIL".to_string();
        }

        let sql_access = self.sql_access.lock().unwrap();

        if sql_access.does_user_exist(&username)
        {
            return "HTTP/1.1 409 Conflict\r\n\r\n".to_string();
        }

        let hashed_password = bcrypt::hash(password, 6).unwrap();
        sql_access.insert_user(&username, &hashed_password, &email);

        msg
    }

    /// Will return A string that holds the HTTP message with the correct response code
    /// 
    /// # Arguments
    /// 
    /// * `username` - A String that holds the username
    /// * `password` - A String that holds the password
    fn login_user(&self, username: String, password: String) -> String
    {
        let msg = "HTTP/1.1 200 OK\r\n\r\n".to_string();

        let sql_access = self.sql_access.lock().unwrap();
        if !sql_access.does_user_exist(&username)
        {
            return "HTTP/1.1 404 NOT FOUND\r\n\r\n".to_string();
        }

        if !sql_access.login_user(&username, &password)
        {
            return "HTTP/1.1 401 Unauthorized\r\n\r\n".to_string();
        }

        msg + &self.add_connected_user(&username, &password)
    }

    /// Returns a String that holds the correct HTTP message
    /// 
    /// # Arguments
    /// * `signature` - A String that holds the signature of the current user (JWT)
    fn logout_user(&self, signature: &String) -> String
    {
        let connected_users = self.connected_users.lock().unwrap();
        let mut users = connected_users.borrow_mut();

        if !users.contains_key(signature)
        {
            return "HTTP/1.1 404 NOT FOUND\r\n\r\n".to_string();
        }

        users.remove(signature);

        "HTTP/1.1 200 OK\r\n\r\n".to_string()
    }

    /// Returns a String with a json that has a signature for the current user
    /// 
    /// # Arguments
    /// 
    /// * `username` - A String reference that holds the username of the current user
    /// * `password` - A String reference that holds the password of the current user
    fn add_connected_user(&self, username: &String, password: &String) -> String
    {
        let user_json: serde_json::Value = serde_json::json!({"username": username, "password": password});;

        let mut rand_key: String = "".to_string();
        let mut rng = rand::thread_rng();

        for _ in 0..26
        {
            let rand_index = rng.gen_range(0, 61);
            rand_key.push(ASCII[rand_index]);
        }

        let token = encode(&Header::default(), &user_json, rand_key.as_ref()).unwrap();
        let parsed_token: Vec<&str> = token.split(".").collect();
        
        let payload = parsed_token[1].to_string();
        let signature = parsed_token[2].to_string();

        let connected_users = self.connected_users.lock().unwrap();
        let mut users = connected_users.borrow_mut();
        users.insert(signature.clone(), payload);

        let signature_json: serde_json::Value = serde_json::json!({"token": signature});

        signature_json.to_string()
    }

    /// Returns a String that holds the correct HTTP code
    /// 
    /// #Arguments
    /// 
    /// * `name` - A String that holds the name of the forum
    /// * `username` - A String that holds the name of the user who created the forum
    /// * `date_created` - A String that holds the date that the forum was created
    /// * `category` - A String that holds the category that the forum is part of
    /// * `content` - A String that holds the content of the forum
    fn create_forum(&self, name: String, author: String, date_created: String, category: String, content: String) -> String
    {
        if name.is_empty() || author.is_empty() || date_created.is_empty() || category.is_empty() || content.is_empty()
        {
            return "HTTP/1.1 404 NOT FOUND\r\n\r\n".to_string();
        }
        if !self.is_category_valid(&category)
        {
            return "HTTP/1.1 406 Not Acceptable\r\n\r\n".to_string();
        }

        let sql_access = self.sql_access.lock().unwrap();
        if sql_access.does_forum_exist(&name, &author)
        {
            return "HTTP/1.1 409 CONFLICT\r\n\r\n".to_string();
        }
        if !sql_access.create_forum(&name, &author, &date_created, &category, &content)
        {
            return "HTTP/1.1 500 Internal Server Error\r\n\r\n".to_string();
        }

        "HTTP/1.1 201 CREATED\r\n\r\n".to_string()
    }

    /// Returns a string with the correct HTTP code
    /// 
    /// # Arguments
    /// * `author` - A String that holds the author's name
    /// * `content` - A String that holds the content
    /// * `date_created` - A String that holds the date that the comment was created, usually will be today
    /// * `forum_id` - An i64 that holds the forum's id so we can know which comment is for which forum
    fn create_forum_comment(&self, author: String, content: String, date_created: String, forum_id: i64) -> String
    {
        if author.is_empty() || content.is_empty() || date_created.is_empty()
        {
            return "HTTP/1.1 404 NOT FOUND\r\n\r\n".to_string()
        }

        let sql_access = self.sql_access.lock().unwrap();
        if !sql_access.create_forum_comment(&author, &content, &date_created, forum_id)
        {
            return "HTTP/1.1 500 Internal Server Error\r\n\r\n".to_string();
        }

        "HTTP/1.1 201 CREATED\r\n\r\n".to_string()
    }

    /// Will handle the GET request
    /// 
    /// # Arguments
    /// 
    /// * `stream` - A TcpStream connection to the user. A mutable refrence so we could borrow and write and read at ease.
    /// * `msg_from_client` - A String that holds the messsage from the client.
    fn handle_get_request(&self, stream: &mut TcpStream, msg_from_client: String)
    {
        let parsed_msg: Vec<&str> = msg_from_client.split(" ").collect();
        let parsed_request: Vec<&str> = parsed_msg[1].split("/").collect();

        let msg_to_client: String = self.handle_get_request_type(parsed_request, &msg_from_client);

        stream.write(msg_to_client.as_bytes()).unwrap();
        stream.flush().unwrap();
    }

    /// Will handle the subtype request (USER, POST, etc.)
    /// 
    /// # Arguments
    /// 
    /// * `parsed_request` - A &str vector that holds the parsed reqeust (The part that comes after the DELETE)
    fn handle_get_request_type(&self, parsed_request: Vec<&str>, msg_from_client: &String) -> String
    {
        let type_of_request = parsed_request[2];

        if type_of_request == "USER"
        {
            let username = parsed_request[3].to_string();
            return self.get_user(username);
        }
        else if type_of_request == "USERS"
        {
            return self.get_all_users();
        }
        else if type_of_request == "USERS_BY_QUERY"
        {
            let query = parsed_request[3].to_string().replace("\'", "\\\'").replace("%20", " ").replace("%27", "\\'").replace("%22", "\\\"");

            return self.get_users_by_search(query);
        }
        else if type_of_request == "FORUM_HEADERS"
        {
            return self.get_forum_headers();
        }
        else if type_of_request == "USER_BY_AUTH"
        {
            let signature = self.get_cookie_value(&msg_from_client);
            return self.get_user_by_auth(signature);
        }
        else if type_of_request == "FORUM_BY_NAME_AND_CREATOR"
        {
            let parsed_data: Vec<&str> = parsed_request[3].split("&").collect();
            let forum_name = parsed_data[0].to_string().replace("%20", " ").replace("%27", "\\'").replace("%22", "\\\"");
            let forum_creator = parsed_data[1].to_string();

            return self.get_forum_by_name_and_creator(forum_name, forum_creator);
        }
        else if type_of_request == "FORUM_ID"
        {
            let parsed_data: Vec<&str> = parsed_request[3].split("&").collect();
            let forum_name = parsed_data[0].to_string().replace("%20", " ").replace("%27", "\\'").replace("%22", "\\\"");
            let forum_creator = parsed_data[1].to_string();

            return self.get_forum_id(forum_name, forum_creator);
        }
        else if type_of_request == "FORUM_COMMENTS"
        {
            let forum_id = parsed_request[3];
            
            return self.get_forum_comments(forum_id.parse::<i64>().unwrap());
        }
        else if type_of_request == "LOGGED_AMOUNT"
        {
            return self.get_logged_amount();
        }
        else if type_of_request == "IS_ADMIN"
        {
            let username = parsed_request[3].to_string();

            return self.is_admin(username);
        }
        else if type_of_request == "FORUM_HEADERS_BY_SEARCH"
        {
            let parsed_data: Vec<&str> = parsed_request[3].split("&").collect();
            let query = parsed_data[0].to_string().replace("\'", "\\\'").replace("%20", " ").replace("%27", "\\'").replace("%22", "\\\"");
            
            let mut category = "".to_string();

            if self.is_category_valid(&parsed_data[1].to_string())
            {
                category = parsed_data[1].to_string();
            }

            return self.get_forum_headers_by_search(query, category);
        }

        "HTTP/1.1 400 BAD REQUEST\r\n\r\n".to_string()
    }
    
    /// Returns a string with the correct HTTP code and the user data in JSON
    /// 
    /// # Arguments
    /// 
    /// * `username` - A string that holds the username of the user we want to get.
    fn get_user(&self, username: String) -> String
    {
        let mut msg = "HTTP/1.1 200 OK\r\n\r\n".to_string();

        let sql_access = self.sql_access.lock().unwrap();
        if !sql_access.does_user_exist(&username)
        {
            return "HTTP/1.1 404 NOT FOUND\r\n\r\n".to_string();
        }

        let user = sql_access.get_user_by_name(&username);

        msg += &serde_json::to_string(&user).unwrap();

        msg
    }

    /// Returns a String with the correct HTTP code and all the users in JSON
    fn get_all_users(&self) -> String
    {
        let sql_access = self.sql_access.lock().unwrap();
        let users = sql_access.get_all_users();
        let msg = "HTTP/1.1 200 OK\r\n\r\n".to_string();

        let json_string = serde_json::to_string(&users).unwrap();
        msg + &json_string
    }

    /// Will get all the users by the query
    /// 
    /// # Arguments
    /// * `query` - A String that holds the query to the DB
    fn get_users_by_search(&self, query: String) -> String
    {
        let msg = "HTTP/1.1 200 OK\r\n\r\n".to_string();

        let sql_access = self.sql_access.lock().unwrap();
        let users = sql_access.get_users_by_search(&query);

        let json_string = serde_json::to_string(&users).unwrap();

        msg + &json_string
    }

    /// Returns a String with the correct HTTP code and all the forum headers in JSON
    fn get_forum_headers(&self) -> String
    {
        let msg = "HTTP/1.1 200 OK\r\n\r\n".to_string();

        let sql_access = self.sql_access.lock().unwrap();
        let headers = sql_access.get_forum_headers();

        let json_string = serde_json::to_string(&headers).unwrap();
        msg + &json_string
    }

    /// Returns a String with the correct HTTP code and the user data in base64
    /// 
    /// # Arguments
    /// 
    /// * `signature` - A String that holds the signature of the user we want to his data
    fn get_user_by_auth(&self, signature: String) -> String
    {
        let msg = "HTTP/1.1 200 OK\r\n\r\n".to_string();

        if signature.is_empty()
        {
            return "HTTP/1.1 404 NOT FOUND\r\n\r\n".to_string();
        }

        let connected_users = self.connected_users.lock().unwrap();
        let users = connected_users.borrow_mut();

        msg + &users.get(&signature).unwrap()
    }

    /// Returns a forum by the forum name and the creator
    /// 
    /// # Arguments
    /// 
    /// * `forum_name` - A String that holds the name of the forum
    /// * `forum_creator` - A String that holds the creator of the forum
    fn get_forum_by_name_and_creator(&self, forum_name: String, forum_creator: String) -> String
    {
        let msg = "HTTP/1.1 200 OK\r\n\r\n".to_string();

        let sql_access = self.sql_access.lock().unwrap();
        if !sql_access.does_forum_exist(&forum_name, &forum_creator)
        {
            return "HTTP/1.1 404 NOT FOUND\r\n\r\n".to_string();
        }

        let forum = sql_access.get_forum_by_name_and_creator(&forum_name, &forum_creator);
        let json_string = serde_json::to_string(&forum).unwrap();

        msg + &json_string
    }

    /// Returns a String that holds the correct HTTP code with the forum ID
    /// 
    /// # Arguments
    /// * `forum_name` - A String that holds the forum's name
    /// * `forum_creator` - A String that holds the forum's creator name
    fn get_forum_id(&self, forum_name: String, forum_creator: String) -> String
    {
        let msg = "HTTP/1.1 200 OK\r\n\r\n".to_string();

        if forum_name.is_empty() || forum_creator.is_empty()
        {
            return "HTTP/1.1 404 NOT FOUND\r\n\r\n".to_string();
        }

        let sql_access = self.sql_access.lock().unwrap();
        let id = sql_access.get_forum_id(&forum_name, &forum_creator);

        let json_data: serde_json::Value = serde_json::json!({"id": id});
        let json_string = serde_json::to_string(&json_data).unwrap();

        msg + &json_string
    }

    /// Returns a String with the correct HTTP code and the forum's comments
    /// 
    /// # Arguments
    /// * `forum_id` - An i64 that holds the requested forum's ID
    fn get_forum_comments(&self, forum_id: i64) -> String
    {
        let msg = "HTTP/1.1 200 OK\r\n\r\n".to_string();

        let sql_access = self.sql_access.lock().unwrap();
        let comments = sql_access.get_forum_comments(forum_id);

        let json_string = serde_json::to_string(&comments).unwrap();
        msg + &json_string
    }

    /// Returns a String with the correct HTTP code and the amount of currently logged users
    fn get_logged_amount(&self) -> String
    {
        let msg = "HTTP/1.1 200 OK\r\n\r\n".to_string();

        let connected_users = self.connected_users.lock().unwrap();
        let users = connected_users.borrow_mut();

        msg + &users.len().to_string()
    }

    /// Returns a String with the correct HTTP code and if the user is an admin or not
    /// 
    /// # Arguments
    /// * `username` - A String that holds the username that we want to check if he's an admin or not
    fn is_admin(&self, username: String) -> String
    {
        let sql_access = self.sql_access.lock().unwrap();

        if !sql_access.does_user_exist(&username)
        {
            return "HTTP/1.1 404 NOT FOUND\r\n\r\n".to_string();
        }

        if !sql_access.is_admin(&username)
        {
            return "HTTP/1.1 401 Unauthorized\r\n\r\n".to_string();
        }

        "HTTP/1.1 200 OK\r\n\r\n".to_string()
    }

    /// Returns a String with the correct HTTP codes and the forum headers based on the query and the category
    /// 
    /// # Arguments
    /// * `query` - A String that holds the query we want to execute in the SQL
    /// * `category` - A String that holds the category of the forums we want to get
    fn get_forum_headers_by_search(&self, query: String, category: String) -> String
    {
        let msg = "HTTP/1.1 200 OK\r\n\r\n".to_string();
        let sql_access = self.sql_access.lock().unwrap();

        let forums = sql_access.get_forum_headers_by_search(&query, &category);

        let json_string = serde_json::to_string(&forums).unwrap();
        msg + &json_string
    }

    /// Will handle the PUT request
    /// 
    /// # Arguments
    /// 
    /// * `stream` - A TcpStream connection to the user. A mutable refrence so we could borrow and write and read at ease.
    /// * `msg_from_client` - A String that holds the messsage from the client.
    fn handle_put_request(&self, stream: &mut TcpStream, msg_from_client: String)
    {
        let parsed_msg: Vec<&str> = msg_from_client.split(" ").collect();
        let parsed_request: Vec<&str> = parsed_msg[1].split("/").collect();

        let msg_to_client: String = self.handle_put_request_type(parsed_request, &msg_from_client);

        stream.write(msg_to_client.as_bytes()).unwrap();
        stream.flush().unwrap();
    }

    /// This function will handle the subtype of the PUT request (change password, edit forum, etc.)
    /// 
    /// # Arguments
    /// 
    /// * `parsed_request` - A &str vector that holds the parsed reqeust (The part that comes after the PUT)
    /// * `msg_from_client` - A String reference that holds the original message we got from the client
    fn handle_put_request_type(&self, parsed_request: Vec<&str>, msg_from_client: &String) -> String
    {
        let type_of_request = parsed_request[2];
        let content_of_msg: Vec<&str> = msg_from_client.split("\r\n\r\n").collect();
        
        let data = content_of_msg[1].to_string().replace("\n", "\\n");
        println!("{}", data);
        let res: serde_json::Value = match serde_json::from_str(&data)
        {
            Ok(json_value) => json_value,
            Err(_) => serde_json::Value::Null
        };

        if res.is_null()
        {
            return "HTTP/1.1 BAD REQUEST\r\n\r\n".to_string();
        }

        if type_of_request == "CHANGE_PASSWORD"
        {
            let username = self.get_username_from_signature(&self.get_cookie_value(msg_from_client));
            let password = res["password"].as_str().unwrap().to_string().replace("\'", "\\\'");
            
            return self.change_password(username, password);
        }
        "HTTP/1.1 400 BAD REQUEST\r\n\r\n".to_string()
    }

    /// Returns the correct HTTP code after it's done changing the password
    /// 
    /// # Arguments
    /// * `username` - A String that holds the username of the user that wants to change the password
    /// * `password` - A String that holds the new password
    fn change_password(&self, username: String, password: String) -> String
    {
        let sql_access = self.sql_access.lock().unwrap();

        if password.len() < 8 || !self.is_valid_password(&password)
        {
            return "HTTP/1.1 412 Precondition Failed\r\n\r\n".to_string();
        }

        let hashed_password = bcrypt::hash(password, 6).unwrap();
        if !sql_access.change_password(&username, &hashed_password)
        {
            return "HTTP/1.1 500 Internal Server Error\r\n\r\n".to_string();
        }

        "HTTP/1.1 200 OK\r\n\r\n".to_string()
    }
}