mod api_handler;

use std::net::TcpListener;
use std::thread;
use std::sync::Arc;

pub struct APIServer
{
    listener: TcpListener,
    handler: Arc<api_handler::APIHandler>
}

impl APIServer
{
    /// Returns a new APIServer instance
    /// 
    /// # Argumnets
    /// 
    /// * `ip` - A String that holds the ip we want to bind the server to
    /// * `port` - An i32 that holds the port that we will bind the server to
    /// * `my_sql_connection_string` - A String that holds the mySql connection string
    pub fn new(ip: String, port: i32, my_sql_connection_string: String) -> APIServer
    {
        let address = format!("{}:{}", ip, port);

        APIServer { listener: TcpListener::bind(address).unwrap(), handler: Arc::new(api_handler::APIHandler::new(my_sql_connection_string)) }
    }

    /// Will listen and create a new thread for each user that enters
    pub fn listen(&self)
    {
        for stream in self.listener.incoming()
        {
            let local_self = self.handler.clone();

            let _ = thread::spawn(move || {
                local_self.handle_client(stream.unwrap());
            });
        }
    }
}